function Person (firstName, lastName, emailAddress, phoneNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.emailAddress = emailAddress;
    this.phoneNumber = phoneNumber;
}

Person.prototype = {
    constructor: Person,
    getFullName: function() {
        return this.firstName + ' ' + this.lastName;
    }
};

var Khai = new Person('khai', 'chang', 'khai.chang89@gmail.com', '715-302-2903');

console.log(Khai.getFullName());